import "./App.css";
import io from "socket.io-client";
import { useEffect, useRef, useState } from "react";

let socket;

function App() {
  const [allMessages, setAllMessages] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const mesRef = useRef();
  const nameRef = useRef();
  const roomRef = useRef();

  useEffect(() => {
    socket = io("localhost:8000", { transports: ["websocket"] });
  }, []);

  useEffect(() => {
    socket.on("message", (msg) => {
      setAllMessages((prev) => [
        ...prev,
        { room: msg.room, user: msg.user, msg: msg.msg, time: msg.time },
      ]);
    });
  }, []);

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    scrollToBottom();
  }, [allMessages]);

  const loginHandler = () => {
    const user = nameRef.current.value;
    const room = roomRef.current.value;

    const userDetails = { user, room };

    socket.emit("join_room", userDetails);
    setIsLoggedIn(true);
  };

  const sendHandler = () => {
    const messageValue = mesRef.current.value;

    const mesContent = {
      msg: messageValue,
    };

    socket.emit("chatMessage", mesContent);
    mesRef.current.value = "";
    mesRef.current.focus();
  };

  return (
    <>
      <div className="App">
        <div id="site-title">
          <h1>Chat App </h1>
        </div>

        <div id="container">
          {!isLoggedIn && (
            <div id="controller">
              <input
                ref={nameRef}
                id="textbox"
                className="form-control"
                rows="2"
                placeholder="Enter your Name!"
              />

              <input
                ref={roomRef}
                id="textbox"
                className="form-control"
                rows="2"
                placeholder="Enter Room"
              />

              <button
                id="send"
                className="btn btn-primary"
                onClick={loginHandler}
              >
                Login
              </button>
            </div>
          )}
          {isLoggedIn && (
            <>
              <div id="main-container">
                <ul>
                  {allMessages.map((s, i) => (
                    <li className="li" key={i}>
                      <div className="user-time">
                        <p>{s.user}</p> <p>{s.time}</p>
                      </div>
                      <div className="message-here">{s.msg}</div>
                    </li>
                  ))}
                  <div ref={messagesEndRef} />
                </ul>
              </div>
              <div id="controller">
                <input
                  ref={mesRef}
                  id="textbox"
                  className="form-control"
                  rows="2"
                  placeholder="Enter your message here"
                />

                <button
                  id="send"
                  className="btn btn-primary"
                  onClick={sendHandler}
                >
                  Send
                </button>
              </div>{" "}
            </>
          )}
        </div>
      </div>
    </>
  );
}

export default App;
