const express = require("express");
const path = require("path");
const app = express();
const port = process.env.PORT || 8000;
const cors = require("cors");
const moment = require("moment");
app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname, "public")));

const http = require("http");
const server = http.createServer(app);

const serverFor = app.listen(port, () =>
  console.log("Listening on port no: ", port)
);

const io = require("socket.io")(serverFor);

io.on("connection", (socket) => {
  socket.on("join_room", (userDettails) => {
    socket.join(userDettails.room);
    socket.emit("message", {
      user: "ChatApp Bot",
      msg: "Welcome to chat-app",
      time: moment().format("h:mm a"),
    });

    console.log(`${userDettails.user} joined ${userDettails.room} room`);

    // New user joined chat
    socket.broadcast.to(userDettails.room).emit("message", {
      user: "ChatApp Bot",
      msg: `${userDettails.user} joined chat`,
      time: moment().format("h:mm a"),
    });

    // Sending msg
    socket.on("chatMessage", (chatMes) => {
      io.to(userDettails.room).emit("message", {
        user: userDettails.user,
        msg: chatMes.msg,
        time: moment().format("h:mm a"),
      });
    });

    // User left the chat
    socket.on("disconnect", () => {
      io.to(userDettails.room).emit("message", {
        user: "ChatApp Bot",
        msg: `${userDettails.user} left the chat!`,
        time: moment().format("h:mm a"),
      });
    });
  });
});
